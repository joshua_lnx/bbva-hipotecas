<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bbva-hipotecas' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'SnzdSW!1HTIY<A5*WaMH j[==>Wv!M!A]44S}&RbQ/p@sxjUr0$qPPA]MtL;|l?x' );
define( 'SECURE_AUTH_KEY',  'eCl@B6W-ESIGCM%UZgR,#Tp=~k22GZr^!fmp6D>q>r.EU1#lZ$[1BYp<}_>O!$>G' );
define( 'LOGGED_IN_KEY',    'HfY1M07O.dOnhysKkTdaI:8bc^CuZ@^i]-zQcI#IK%g:d,HzG+HW|L)ZPbS`lp)z' );
define( 'NONCE_KEY',        'P{4M~jU#Sd{PdZl[[f/rx3Hj&:cU2YBQ),hMF+/|l +siWv-Wt_J(snsp.G0g_0i' );
define( 'AUTH_SALT',        'Fj6Px^GRc;SR/Z*ap|7xI2$uTOD?[$@#%D$n&kci.Xg64Gysb?y(&u #i?:eN4g`' );
define( 'SECURE_AUTH_SALT', 'zk}^l}$w<pdP8:;=`pJ{tS?lXba^2a6Xcg>XMQw~<dXE^Vp,b.,8(ZZ0&*bP<%9a' );
define( 'LOGGED_IN_SALT',   'BVGOM{D4;<%(IIN/irrrF`|8CTwsOKIx,e=W1]+KqH77ZA5t-}Cvp.mr!6E4OtX<' );
define( 'NONCE_SALT',       'hD`Mtn/rQjDbVFpd>a|J-xC(tSP/6}F1~LpwZa0^YkPw~9s+MK{=+=(Su.ZI(FbR' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
