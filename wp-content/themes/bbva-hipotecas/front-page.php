<?php get_header(); ?>

<?php

 // Consulta a base de datos para mandar a llamar un numero de entradas
   $args = array(
    'posts_per_page' => 3
   );
?>

<!-- Entradas -->
<div class="container my-5">
    <h1 class="title-links mb-5 text-center">Noticias relevantes</h1>
    <div class="row">
        <?php $entradas = new WP_Query($args); while ($entradas->have_posts() ): $entradas->the_post();?>
            <div class="col-12 col-sm-6 col-md-4 mb-3">
                <div class="card bg-card-ligth text-dark">
                    <?php
                        if( has_post_thumbnail()) {
                            the_post_thumbnail('post-thumbnails', array(
                                'class' => 'card-img-top img-fluid'
                            ));
                        }
                    ?>
                    <div class="card-body">
                        <h4 class="card-title"> <?php the_title(); ?> </h4>
                        <?php if ( ! has_excerpt() ) {
                                    echo '';
                            } else {
                                the_excerpt();
                            }
                        ?>
                        <?php  echo '<p class="card-text my-3">' . get_the_content() . '</p>' ?>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
</div>
<!-- Entradas -->

<!-- Comunicados -->
<!-- Comunicados -->

<? get_footer(); ?>