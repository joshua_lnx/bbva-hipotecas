<?php

// Add Bootstrap css
  function add_theme_style() {
    wp_enqueue_style( 'style', get_stylesheet_uri());
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_script( 'popper_js',
  					'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js',
  					array(),
  					'1.14.3',
  					true);
	wp_enqueue_script( 'bootstrap_js',
  					'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js',
  					array('jquery','popper_js'),
  					'4.1.3',
  					true);
  }
  add_action( 'wp_enqueue_scripts', 'add_theme_style' );

  // Función para registrar menus
  function register_menus() {
    register_nav_menus(
      array(
        'main-menu' => __( 'Menú principal' )
      )
    );
  }
  add_action( 'init', 'register_menus' );

  // Funcion para agregar clase a las anclas del menu
  function add_specific_menu_location_atts( $atts, $item, $args ) {
    if( $args->theme_location == 'main-menu' ) {
      $atts['class'] = 'nav-item nav-link';
    }
    return $atts;
  }
  add_filter( 'nav_menu_link_attributes', 'add_specific_menu_location_atts', 10, 3 );


  // imagen destacada
  if( function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
  }

  /*
     Registro un custom post type 'Documentos'.
  */
  function bf_register_custom_post_type() {
      /* Añado las etiquetas que aparecerán en el escritorio de WordPress */
    $labels = array(
      'name'               => _x( 'Documentos', 'post type general name', 'text-domain' ),
      'singular_name'      => _x( 'Documentos', 'post type singular name', 'text-domain' ),
      'menu_name'          => _x( 'Documentos', 'admin menu', 'text-domain' ),
      'add_new'            => _x( 'Añadir nuevo', 'Documento', 'text-domain' ),
      'add_new_item'       => __( 'Añadir nuevo Documento', 'text-domain' ),
      'new_item'           => __( 'Nuevo Documento', 'text-domain' ),
      'edit_item'          => __( 'Editar Documento', 'text-domain' ),
      'view_item'          => __( 'Ver Documento', 'text-domain' ),
      'all_items'          => __( 'Todos los Documentos', 'text-domain' ),
      'search_items'       => __( 'Buscar Documentos', 'text-domain' ),
      'not_found'          => __( 'No hay Documentos.', 'text-domain' ),
      'not_found_in_trash' => __( 'No hay Documentos en la papelera.', 'text-domain' ),
    );

      /* comportamiento y funcionalidades del nuevo custom post type */
    $args = array(
      'labels'             => $labels,
      'menu_icon'          => __( 'dashicons-media-document' ),
      'description'        => __( 'Descripción.', 'text-domain' ),
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'documentos' ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );

    register_post_type( 'documentos', $args );
  }
  add_action( 'init', 'bf_register_custom_post_type' );