<!DOCTYPE html>
<html lang="<?php bloginfo('language'); ?>">

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php bloginfo('name'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>">
    <?php wp_head(); ?>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-bbva" data-spy="affix">
        <div class="container">
            <a class="navbar-brand" href="#">BBVA</a>
            <button
                class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarColor01"
                aria-controls="navbarColor01"
                aria-expanded="false"
                aria-label="Toggle navigation"
                >
                <span class="navbar-toggler-icon"></span>
            </button>
            <?php
                wp_nav_menu( array(
                    'theme_location'    =>  'main-menu',
                    'container'         =>  'div',
                    'container_class'   =>  'collapse navbar-collapse',
                    'container_id'      =>  'navbarColor01',
                    'menu_class'        =>  'nav navbar-nav',
                    'items_wrap'        =>  '<ul class="navbar-nav mr-auto">%3$s</ul>'
                ) );
            ?>
        </div>
    </nav>