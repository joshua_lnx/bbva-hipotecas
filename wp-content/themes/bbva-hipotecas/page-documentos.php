<?php
    /*
        Template Name: Plantilla Documentos
    */
?>

<?php get_header(); ?>

<body <?php body_class() ?>>
<div class="container-fluid contenedor-noticias">
    <?php
        if( has_post_thumbnail()) {
            echo '<h1 class="text-center text-white position-absolute">' . get_the_title() . '</h1>';
            the_post_thumbnail('post-thumbnails', array(
                'class' => 'img-fluid'
            ));
        }else {
            echo '<h1 class="title-docs my-5">' . get_the_title() . '</h1>';
        }
    ?>
</div>
<?php
    $args = array(
        'post_type' => 'documentos',
        'post_per_page' => 3
    );
    $documentos = new WP_Query($args);
    if($documentos->have_posts() ):
        while ($documentos->have_posts() ): $documentos->the_post();?>
            <div class="container my-5">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="content-text">
                            <h3 class="my-3 title-documents"> <?php the_title(); ?> </h3>
                            <?php if ( ! has_excerpt() ) {
                                    echo '';
                                } else {
                                    echo '<p class="the-excerpt">' . get_the_excerpt() . '</p>';
                                }
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="card card-doc w-100 my-4 position-relative">
                            <div class="card-body my-3">
                                <h6 class="card-title mx-4 mb-3">Documentos</h6>
                                <?php  echo '<div class="card-text my-3">' . get_the_content() . '</div>' ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
        <?php endif; ?>

<?php  get_footer( );?>