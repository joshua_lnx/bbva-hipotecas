<?php
    /*
        Template Name: Plantilla noticias
    */
?>

<?php get_header(); ?>

<div class="container-fluid contenedor-noticias">
    <?php
        if( has_post_thumbnail()) {
            echo '<h1 class="text-center text-white position-absolute">' . get_the_title() . '</h1>';
            the_post_thumbnail('post-thumbnails', array(
                'class' => 'img-fluid'
            ));
        }else {
            echo '<h1 class="title-docs my-5">' . get_the_title() . '</h1>';
      }
    ?>
</div>

<!-- Consulta a base de datos para mandar a llamar un numero de entradas -->
<?php
   $args = array(
    'posts_per_page' => 99
   );

   $entradas = new WP_Query($args);
?>

<!-- Entradas -->
<div class="container my-5">
    <div class="row">
        <?php while ($entradas->have_posts() ): $entradas->the_post();?>
        <div class="col-12 col-sm-6 col-md-4 mb-3">
            <div class="card bg-card-ligth text-dark">
                <?php
                        if( has_post_thumbnail()) {
                            the_post_thumbnail('post-thumbnails', array(
                                'class' => 'card-img-top img-fluid'
                            ));
                        }
                    ?>
                <div class="card-body">
                    <h4 class="card-title"> <?php the_title(); ?> </h4>
                    <?php if ( ! has_excerpt() ) {
                                    echo '';
                            } else {
                                the_excerpt();
                            }
                        ?>
                    <?php  echo '<p class="card-text my-3">' . get_the_content() . '</p>' ?>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
</div>
<!-- Entradas -->
<div class="container my-5">
    <?php while (have_posts() ): the_post();?>
        <?php the_content(); ?>
    <?php endwhile; ?>
</div>

<?php get_footer(); ?>