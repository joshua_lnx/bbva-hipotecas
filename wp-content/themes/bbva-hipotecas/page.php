
<?php get_header(); ?>

<div class="container-fluid contenedor-noticias">
  <h1 class="text-center text-white position-absolute"> <?php the_title(); ?></h1>
  <?php
      if( has_post_thumbnail()) {
          the_post_thumbnail('post-thumbnails', array(
              'class' => 'img-fluid'
          ));
      }else {
          the_title();
    }
  ?>
</div>

<div class="container my-5">
  <?php while (have_posts() ): the_post();?>
    <?php the_content(); ?>
  <?php endwhile; ?>
</div>

<?php get_footer(); ?>