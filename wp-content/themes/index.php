<?php
/**
 * The main template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package OpenWeb
 * @subpackage Theme
 * @since 1.0
 * @version 1.0
 */
require_once __DIR__.'/vendor/autoload.php';
$theme = \OpenWeb\Theme::getInstance();

$articles = [];

while (have_posts()) {
    the_post();
    $articles[] = get_post();
}

get_header();
?>

<link href="<?php echo get_template_directory_uri();?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"
    id="bootstrap-css">

<script src="<?php echo get_template_directory_uri();?>/assets/bootstrap/js/bootstrap.min.js"></script>

<?php
$post_id = 158;

/*ID pagina Servidor
$post_id = 158;
*/

    $Imagen1 = get_field('imagen_1', $post_id);
    $Imagen2 = get_field('imagen_2', $post_id);
    $Imagen3 = get_field('imagen_3', $post_id);

    $Titulo1 = get_field('titulo_1', $post_id);
    $Titulo2 = get_field('titulo_2', $post_id);
    $Titulo3 = get_field('titulo_3', $post_id);

    $imagen1 = $Imagen1;
    $imagen2 = $Imagen2;
    $imagen3 = $Imagen3;


?>

<?php  if( !empty($imagen1) ):  ?>
<div class="container-fluid container-banner">
    <!-- Carousel -->
    <div id="carousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class="active"></li>
            <?php  if( !empty($imagen2) ):  ?>
            <li data-target="#carousel" data-slide-to="1"></li>
            <?php endif; ?>

            <?php  if( !empty($imagen3) ):  ?>
            <li data-target="#carousel" data-slide-to="2"></li>
            <?php endif; ?>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="<?php echo $Imagen1; ?>" alt="Slide1">
                <!-- Static Header -->
                <div class="header-text hidden-xs">
                    <div class="col-md-9">
                        <div class="col-sm-5 card-hero-home">
                            <div class="card-text">
                                <?php echo $Titulo1; ?>
                            </div>
                        </div>
                    </div>
                </div><!-- /header-text -->
            </div>
            <?php  if( !empty($imagen2) ):  ?>
            <div class="item">
                <img src="<?php echo $Imagen2; ?>" alt="Slide2">
                <!-- Static Header -->
                <div class="header-text hidden-xs">
                    <div class="col-md-9">
                        <div class="col-sm-5 card-hero-home">
                            <div class="card-text">
                                <p class="text-hero-banner">En <span class="text-medium">BBVA</span> estamos para
                                    ayudarte</p>
                                <p class="disclosure">Todo lo que necesitas para tu hipoteca</p>
                            </div>
                        </div>
                    </div>
                </div><!-- /header-text -->
            </div><?php endif; ?>
            <?php  if( !empty($imagen3) ):  ?>
            <div class="item">
                <img src="<?php echo $Imagen3; ?>" alt="Slide3">
                <!-- Static Header -->
                <div class="header-text hidden-xs">
                    <div class="col-md-9">
                        <div class="col-sm-5 card-hero-home">
                            <div class="card-text">
                                <p class="text-hero-banner">En <span class="text-medium">BBVA</span> estamos para
                                    ayudarte</p>
                                <p class="disclosure">Todo lo que necesitas para tu hipoteca</p>
                            </div>
                        </div>

                    </div>
                </div><!-- /header-text -->
            </div><?php endif; ?>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div><!-- /carousel -->
</div><?php endif; ?>


<section class="container-fluid bg-grey100">
    <div>
        <?php $theme->renderView('loop/structure', $articles); ?>
    </div>

</section>

<?php get_footer();