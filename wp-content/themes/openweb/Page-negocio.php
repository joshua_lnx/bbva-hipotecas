

<?php

/*
* Template Name: Plantilla Negocio Promotor 
*/
 get_header();



// Optener la pagina Actual

$paged1 = isset( $_GET['paged1'] ) ? (int) $_GET['paged1'] : 1;


   

 $categoria1 = 'negocio';
 $taxonomia1 = 'Categoria_negocio';
 $termino1 = 'credito-puente';
    $args1 = array(
        'post_type' => $categoria1, // the post type
        'posts_per_page'=>'2',
        'paged'         => $paged1,
        
        'tax_query' => array(
            array(
                'taxonomy' => $taxonomia1, // the custom vocabulary
                'field'    => 'slug',                 
                'terms'    => array($termino1),      // provide the term slugs
            ),
        ),
    );




$paged2 = isset( $_GET['paged2'] ) ? (int) $_GET['paged2'] : 1;

$categoria2 = 'negocio';
$taxonomia2 = 'Categoria_negocio';
$termino2 = 'prepuente';
  $args2 = array(
        'post_type' => $categoria2, // the post type
        'posts_per_page'=>'2',
        'paged'     => $paged2,
       
        'tax_query' => array(
            array(
                'taxonomy' => $taxonomia2, // the custom vocabulary
                'field'    => 'slug',                 
                'terms'    => array($termino2),      // provide the term slugs
            ),
        ),
    );
          


$paged3 = isset( $_GET['paged3'] ) ? (int) $_GET['paged3'] : 1;

$categoria3 = 'negocio';
$taxonomia3 = 'Categoria_negocio';
$termino3 = 'reserva-territorial';
  $args3 = array(
        'post_type' => $categoria3, // the post type
        'posts_per_page'=>'2',
        'paged'     => $paged3,
       
        'tax_query' => array(
            array(
                'taxonomy' => $taxonomia3, // the custom vocabulary
                'field'    => 'slug',                 
                'terms'    => array($termino3),      // provide the term slugs
            ),
        ),
    );
          
$paged4 = isset( $_GET['paged4'] ) ? (int) $_GET['paged4'] : 1;

$categoria4 = 'negocio';
$taxonomia4 = 'Categoria_negocio';
$termino4 = 'urbanizacion-e-infraestructura';
  $args4 = array(
        'post_type' => $categoria4, // the post type
        'posts_per_page'=>'2',
        'paged'     => $paged4,
       
        'tax_query' => array(
            array(
                'taxonomy' => $taxonomia4, // the custom vocabulary
                'field'    => 'slug',                 
                'terms'    => array($termino4),      // provide the term slugs
            ),
        ),
    );



?>



<?php get_header(); ?>



<div class="hero-banner2">
<img src="<?php echo get_the_post_thumbnail_url (); ?>">

</div>



<section>

    <div class="container-fluid bg-primary ">





        <div class="container my-5">
            <div class="row">
                <div class="col-sm-3">
                    <div class="content-text">
                        <div class="bbva-coronita_profile blue-medium my-6 pt30"></div>
                        
                        <h2 class="thin"> <br>Credito Puente</h2>

                     
                    </div>
                </div>
                <div class="col-sm-9 white mx-6">
                	<div class="bg-white">
                    <div class="card-body my-3 bbva-cards"></div>

                    <h6 class="card-title mx-4 mb-4 h6 bullet categoria-9 ":after> Documentos</h6>
                    	<ul>



                        <?php 
                                  
                         
                           $query1 = new WP_Query ( $args1  );
                            if ( $query1->have_posts() ) : 
                            ?>
                                 <div class="documento">
                                    <?php while ( $query1->have_posts() ) : $query1->the_post(); ?>
                                        <li><?php the_title(); ?>
                                         <a href="<?php the_field('descarga'); ?>"target="_blank"><i class="bbva-coronita_download pl10 descarga"></a></i></li>
                                    <?php endwhile; ?>
                                </div>
                                  <?php endif; ?>
                            
                                </ul><?php 

                                $pag_args1 = array(
                                        'format'  => '?paged1=%#%',
                                        'current' => $paged1,
                                        'total'   => $query1->max_num_pages,
                                        'add_args' => array( 'paged' => $paged1 )
                                    );



                                echo paginate_links( $pag_args1 ); ?>


                        </div></div>
							 

                </div>


            </div>
        </div></div>
</section>


<section>

    <div class="container-fluid bg-primary ">

        <div class="container my-5">
            <div class="row">
                <div class="col-sm-3">
                    <div class="content-text">
                        <div class="bbva-coronita_agenda blue-medium my-6 pt30"></div>
                        
                        <h2 class="thin"> <br>Prepuente</h2>

                     
                    </div>
                </div>
                <div class="col-sm-9 white mx-6">
                    <div class="card-body my-3 bbva-cards"></div>
<div class="bg-white">
                    <h6 class="card-title mx-4 mb-4 h6 bullet categoria-9 ":after>Documentos</h6>
                    	<ul>
                    
                        <?php 
                            $query2 = new WP_Query ( $args2 );
                            if ( $query2->have_posts() ) : 
                            ?>
                                 <div class="documento">
                                    <?php while ( $query2->have_posts() ) : $query2->the_post(); ?>
                                        <li><?php the_title(); ?>
                                         <a href="<?php the_field('descarga'); ?>"target="_blank"><i class="bbva-coronita_download pl10 descarga"></a></i></li>
                                    <?php endwhile; ?>
                                </div>
                                  <?php endif; ?>
                            
                                </ul><?php 

                                $pag_args2 = array(
                                        'format'  => '?paged2=%#%',
                                        'current' => $paged2,
                                        'total'   => $query2->max_num_pages,
                                        'add_args' => array( 'paged' => $paged2 )
                                    );



                                echo paginate_links( $pag_args2 ); ?>
         

</div></div>

                </div>

                

            </div>
        </div></div>

</section>



</section>

<section>

    <div class="container-fluid bg-primary ">

        <div class="container my-5">
            <div class="row">
                <div class="col-sm-3">
                    <div class="content-text">
                        <div class="bbva-coronita_profile blue-medium my-6 pt30"></div>
                        
                        <h2 class="thin"> <br>Reserva Territorial</h2>

                     
                    </div>
                </div>
                <div class="col-sm-9 white mx-6">
                	<div class="bg-white">
                    <div class="card-body my-3 bbva-cards"></div>

                    <h6 class="card-title mx-4 mb-4 h6 bullet categoria-9 ":after> Documentos</h6>
                    <ul>


                        <?php 
                           $query3 = new WP_Query ( $args3 );
                            if ( $query3->have_posts() ) : 
                            ?>
                                 <div class="documento">
                                    <?php while ( $query3->have_posts() ) : $query3->the_post(); ?>
                                        <li><?php the_title(); ?>
                                         <a href="<?php the_field('descarga'); ?>"target="_blank"><i class="bbva-coronita_download pl10 descarga"></a></i></li>
                                    <?php endwhile; ?>
                                </div>
                                  <?php endif; ?>
                            
                                </ul><?php 

                                $pag_args3 = array(
                                        'format'  => '?paged3=%#%',
                                        'current' => $paged3,
                                        'total'   => $query3->max_num_pages,
                                        'add_args' => array( 'paged' => $paged3 )
                                    );



                                echo paginate_links( $pag_args3 ); ?>



                </div></div>
                             

                </div>

            </div>
        </div></div>

</section>

<section>

    <div class="container-fluid bg-primary ">

        <div class="container my-5">
            <div class="row">
                <div class="col-sm-3">
                    <div class="content-text">
                        <div class="bbva-coronita_profile blue-medium my-6 pt30"></div>
                        
                        <h2 class="thin"> <br>Urbanización e Infraestructura</h2>

                     
                    </div>
                </div>
                <div class="col-sm-9 white mx-6">
                    <div class="bg-white">
                    <div class="card-body my-3 bbva-cards"></div>

                    <h6 class="card-title mx-4 mb-4 h6 bullet categoria-9 ":after> Documentos</h6>
                    <ul>


                        <?php 
                           $query4 = new WP_Query ( $args4 );
                            if ( $query4->have_posts() ) : 
                            ?>
                                 <div class="documento">
                                    <?php while ( $query4->have_posts() ) : $query4->the_post(); ?>
                                        <li><?php the_title(); ?>
                                         <a href="<?php the_field('descarga'); ?>"target="_blank"><i class="bbva-coronita_download pl10 descarga"></a></i></li>
                                    <?php endwhile; ?>
                                </div>
                                  <?php endif; ?>
                            
                                </ul><?php 

                                $pag_args4 = array(
                                        'format'  => '?paged3=%#%',
                                        'current' => $paged4,
                                        'total'   => $query4->max_num_pages,
                                        'add_args' => array( 'paged' => $paged4 )
                                    );



                                echo paginate_links( $pag_args4 ); ?>



                </div></div>
                             

                </div>

            </div>
        </div></div>

</section>



</div></div>


</div><!-- #primary -->
</div><!-- .wrap -->
</article>
</div>
</section>



<?php get_footer();
