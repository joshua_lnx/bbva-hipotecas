<?php

/*
* Template Name: Plantilla Esquema/Formatos
*/
 get_header();



?>



<?php
$categoria = get_field('nombre_categoria');
    $args = array(
        'post_type' => 'esquema', // the post type
        'tax_query' => array(
            array(
                'taxonomy' => 'Categorias_esquema', // the custom vocabulary
                'field'    => 'slug',
                'terms'    => array($categoria),      // provide the term slugs
            ),
        ),
    );

 ?>



<section>

    <div class="container-fluid bg-primary ">
        <div class="container my-5">
            <div class="row">
                <div class="col-sm-3">
                    <div class="content-text">
                        <div class="bbva-coronita_profile blue-medium my-6 pt30"></div>

                        <h2 class="thin"> <br><?php echo get_the_title(); ?></h2>


                    </div>
                </div>
                <div class="col-sm-9 white mx-6">
                    <div class="bg-white">
                        <div class="card-body my-3 bbva-cards"></div>

                        <h6 class="card-title mx-4 mb-4 h6 bullet categoria-9 " :after> Documentos</h6>
                        <ul><?php
                                    $pquery = new WP_Query ($args);
                                    if($pquery->have_posts()):
                                    while($pquery->have_posts()):
                                    $pquery->the_post();


                         ?>

                            <div class="documento">
                                <li> <?php the_title(''); ?>
                                    <a href="<?php the_field('descarga'); ?>" target="_blank"><i
                                            class="bbva-coronita_download pl10 descarga"></a></i></li>
                            </div>

                            <?php   
                                        endwhile;   
                            


                                        wp_reset_postdata ();   


                                

                                        else :

                                        endif; 
                                        
                                ?></ul>
                    </div>


                </div>



            </div>
        </div>
    </div>

</section>





<?php  if (have_posts()): ?>
<?php   while(have_posts()): the_post(); ?>


</section>








</div>
</div>


</div><!-- #primary -->
</div><!-- .wrap -->
</article>
</div>
</section>


<?php endwhile;?>
<?php endif;?>

<?php get_footer();