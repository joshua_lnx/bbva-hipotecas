<?php

/**
 * OpenWeb functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package OpenWeb
 * @subpackage Theme
 * @since Theme 1.0
 */
if (version_compare(PHP_VERSION, '5.6', '<')) {
    die(__('El tema Openweb basado en Coronita sólo funciona a partir de las versiones 5.6.x de PHP', 'openweb'));
}

if (version_compare($GLOBALS['wp_version'], '4.8', '<')) {
    die(__('El tema Openweb basado en Coronita sólo funciona a partir de las versiones 4.8 de Wordpress', 'openweb'));
}

define('OPENWEB_THEME_PATH', __DIR__);

require_once OPENWEB_THEME_PATH.'/vendor/autoload.php';

$theme = \OpenWeb\Theme::getInstance();
$theme->init();


function revealid_add_id_column( $columns ) {
    $checkbox = array_slice( $columns , 0, 1 );
    $columns = array_slice( $columns , 1 );

    $id['revealid_id'] = 'ID';
    
    $columns = array_merge( $checkbox, $id, $columns );
    return $columns;
}

function noticias_register_custom_post_type() {
    /* Añado las etiquetas que aparecerán en el escritorio de WordPress */
  $labels2 = array(
    'name'               => _x( 'Noticias', 'post type general name', 'text-domain' ),
    'singular_name'      => _x( 'Noticias', 'post type singular name', 'text-domain' ), 
    'menu_name'          => _x( 'Noticias', 'admin menu', 'text-domain' ),
    'add_new'            => _x( 'Añadir nueva', 'Noticia', 'text-domain' ),
    'add_new_item'       => __( 'Añadir nueva Noticia', 'text-domain' ),
    'new_item'           => __( 'Nueva Noticia', 'text-domain' ),
    'edit_item'          => __( 'Editar Noticia', 'text-domain' ),
    'view_item'          => __( 'Ver Noticia', 'text-domain' ),
    'all_items'          => __( 'Todos las Noticias', 'text-domain' ),
    'search_items'       => __( 'Buscar Noticias', 'text-domain' ),
    'not_found'          => __( 'No hay Noticias.', 'text-domain' ),
    'not_found_in_trash' => __( 'No hay Noticias en la papelera.', 'text-domain' ),
  );

    /* comportamiento y funcionalidades del nuevo custom post type */
  $args2 = array(
    'labels'             => $labels2,
    'menu_icon'          => __( 'dashicons-media-document' ),
    'description'        => __( 'Descripción.', 'text-domain' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'noticias' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
  );

  register_post_type( 'Noticias1', $args2 );
}


add_action( 'init', 'noticias_register_custom_post_type' );



// Creacion Documentos Custom Post Type
function docs_custom_post_type() {
    $docs = array(
        
        'parent_item_colon'   => __( 'Parent Documentos'),
     
        'name'               => _x( 'Reportes', 'post type general name', 'text-domain' ),
        'singular_name'      => _x( 'Reportes', 'post type singular name', 'text-domain' ),
        'menu_name'          => _x( 'Reportes', 'admin menu', 'text-domain' ),
        'add_new'            => _x( 'Añadir nuevo', 'Documento', 'text-domain' ),
        'add_new_item'       => __( 'Añadir nuevo Documento', 'text-domain' ),
        'new_item'           => __( 'Nuevo Documento', 'text-domain' ),
        'update_item'         => __( 'Actualizar Documento'),
        'edit_item'          => __( 'Editar Documento', 'text-domain' ),
        'view_item'          => __( 'Ver Documento', 'text-domain' ),
        'all_items'          => __( 'Todos los Documentos', 'text-domain' ),
        'search_items'       => __( 'Buscar Documentos', 'text-domain' ),
        'not_found'          => __( 'No hay Documentos.', 'text-domain' ),
        'not_found_in_trash' => __( 'No hay Documentos en la papelera.', 'text-domain' ),
    );

    $args = array(
        'label'               => __( 'Documentos'),
        'description'         => __( 'Documentos General'),
        'labels'              => $docs,
        'menu_icon'           => __( 'dashicons-media-spreadsheet' ),
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'revisions', 'custom-fields'),
        'public'              => true,
        'hierarchical'        => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'has_archive'         => true,
        'can_export'          => true,
        'exclude_from_search' => false,
            'yarpp_support'       => true,
        'taxonomies'          => array('post_tag'),
        'publicly_queryable'  => true,
        'capability_type'     => 'page'
);
    register_post_type( 'docu1', $args );
}
add_action( 'init', 'docs_custom_post_type', 0 );


// Let us create Taxonomy for Custom Post Type
add_action( 'init', 'docs_custom_taxonomy', 0 );
 
//create a custom taxonomy name it "type" for your posts
function docs_custom_taxonomy() {
 
  $labels = array(
    'name' => _x( 'Categorias', 'taxonomy general name' ),
    'singular_name' => _x( 'Categoria', 'taxonomy singular name' ),
    'search_items' =>  __( 'Buscar Categorias' ),
    'all_items' => __( 'Todas las Categorias' ),
    'parent_item' => __( 'Categoria Superior' ),
    'parent_item_colon' => __( 'Categoria Superior:' ),
    'edit_item' => __( 'Editar Categoria' ), 
    'update_item' => __( 'Actualizar Categoria' ),
    'add_new_item' => __( 'Agregar Nueva Categoria' ),
    'new_item_name' => __( 'Nombre Nueva Categoria ' ),
    'menu_name' => __( 'Categorias' ),
  );    
 
  register_taxonomy('Categorias',array('docu1'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'docu1' ),
  ));
}



// Creación Productos Custom Post Type
function productos1_custom_post_type() {
    $producs = array(
        
        'parent_item_colon'   => __( 'Parent Productos1'),
     
        'name'              => _x( 'Productos', 'post type general name', 'text-domain' ),
        'singular_name'      => _x( 'Productos1', 'post type singular name', 'text-domain' ),
        'menu_name'          => _x( 'Productos', 'admin menu', 'text-domain' ),
        'add_new'            => _x( 'Añadir nuevo', 'Producto', 'text-domain' ),
        'add_new_item'       => __( 'Añadir nuevo Producto', 'text-domain' ),
        'new_item'           => __( 'Nuevo Producto', 'text-domain' ),
        'update_item'        => __( 'Actualizar Producto'),
        'edit_item'          => __( 'Editar Producto', 'text-domain' ),
        'view_item'          => __( 'Ver Producto', 'text-domain' ),
        'all_items'          => __( 'Todos los Productos', 'text-domain' ),
        'search_items'       => __( 'Buscar Productos', 'text-domain' ),
        'not_found'          => __( 'No hay Productos.', 'text-domain' ),
        'not_found_in_trash' => __( 'No hay Productos en la papelera.', 'text-domain' ),
    );

    $args_productos = array(
        'label'               => __( 'Productos1'),
        'description'         => __( 'Area para Productos'),
        'labels'              => $producs,
        'menu_icon'           => __( 'dashicons-performance' ),
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'revisions', 'custom-fields'),
        'public'              => true,
        'hierarchical'        => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'has_archive'         => true,
        'can_export'          => true,
        'exclude_from_search' => false,
        'yarpp_support'       => true,
      
        'publicly_queryable'  => true,
        'capability_type'     => 'page'
);
    register_post_type( 'productos1', $args_productos );
}
add_action( 'init', 'productos1_custom_post_type', 0 );

add_action( 'init', 'productos1_custom_taxonomy', 0 );


function productos1_custom_taxonomy() {
 
  $labels_producs = array(
    'name' => _x( 'Categorias', 'taxonomy general name' ),
    'singular_name' => _x( 'Categoria', 'taxonomy singular name' ),
    'search_items' =>  __( 'Buscar Categorias' ),
    'all_items' => __( 'Todas las Categorias' ),
    'parent_item' => __( 'Categoria Superior' ),
    'parent_item_colon' => __( 'Categoria Superior:' ),
    'edit_item' => __( 'Editar Categoria' ), 
    'update_item' => __( 'Actualizar Categoria' ),
    'add_new_item' => __( 'Agregar Nueva Categoria' ),
    'new_item_name' => __( 'Nombre Nueva Categoria ' ),
    'menu_name' => __( 'Categorias' ),
  );    
 
  register_taxonomy('Categoria_productos',array('productos1'), array(
    'hierarchical' => true,
    'labels' => $labels_producs,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'productos1' ),
  ));
}



// Post personalizado Mercado
function mercado_custom_post_type() {
    $mercado = array(
        
        'parent_item_colon'   => __( 'Parent Mercado'),
     
        'name'               => _x( 'Mercado', 'post type general name', 'text-domain' ),
        'singular_name'      => _x( 'Mercado', 'post type singular name', 'text-domain' ),
        'menu_name'          => _x( 'Mercado', 'admin menu', 'text-domain' ),
        'add_new'            => _x( 'Añadir nuevo', 'Producto', 'text-domain' ),
        'add_new_item'       => __( 'Añadir nuevo Producto', 'text-domain' ),
        'new_item'           => __( 'Nuevo Producto', 'text-domain' ),
        'update_item'         => __( 'Actualizar Producto'),
        'edit_item'          => __( 'Editar Producto', 'text-domain' ),
        'view_item'          => __( 'Ver Producto', 'text-domain' ),
        'all_items'          => __( 'Todos los Mercado', 'text-domain' ),
        'search_items'       => __( 'Buscar Mercado', 'text-domain' ),
        'not_found'          => __( 'No hay Mercado.', 'text-domain' ),
        'not_found_in_trash' => __( 'No hay Mercado en la papelera.', 'text-domain' ),
    );

    $args_mercado = array(
        'label'               => __( 'Mercado y Competencia'),
        'description'         => __( 'Area para Productos'),
        'labels'              => $mercado,
        'menu_icon'           => __( 'dashicons-chart-area' ),
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'revisions', 'custom-fields'),
        'public'              => true,
        'hierarchical'        => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'has_archive'         => true,
        'can_export'          => true,
        'exclude_from_search' => false,
        'yarpp_support'       => true,
      
        'publicly_queryable'  => true,
        'capability_type'     => 'page'
);
    register_post_type( 'mercado', $args_mercado );
}
add_action( 'init', 'mercado_custom_post_type', 0 );



add_action( 'init', 'mercado_custom_taxonomy', 0 );


function mercado_custom_taxonomy() {
 
  $labels_mercado = array(
    'name' => _x( 'Categorias_mercado', 'taxonomy general name' ),
    'singular_name' => _x( 'Categoria', 'taxonomy singular name' ),
    'search_items' =>  __( 'Buscar Categorias' ),
    'all_items' => __( 'Todas las Categorias' ),
    'parent_item' => __( 'Categoria Superior' ),
    'parent_item_colon' => __( 'Categoria Superior:' ),
    'edit_item' => __( 'Editar Categoria' ), 
    'update_item' => __( 'Actualizar Categoria' ),
    'add_new_item' => __( 'Agregar Nueva Categoria' ),
    'new_item_name' => __( 'Nombre Nueva Categoria ' ),
    'menu_name' => __( 'Categorias' ),
  );    
 
  register_taxonomy('Categoria_mercado',array('mercado'), array(
    'hierarchical' => true,
    'labels' => $labels_mercado,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'mercado' ),
  ));
}






// Post personalizado Negocio Promotor
function Negocio_custom_post_type() {
    $negocio = array(
        'parent_item_colon'   => __( 'Parent Negocio_Promotor'),
        'name'               => _x( 'Negocio Promotor', 'post type general name', 'text-domain' ),
        'singular_name'      => _x( 'Negocio Promotor', 'post type singular name', 'text-domain' ),
        'menu_name'          => _x( 'Negocio Promotor', 'admin menu', 'text-domain' ),
        'add_new'            => _x( 'Añadir nuevo', 'Documento', 'text-domain' ),
        'add_new_item'       => __( 'Añadir nuevo Documento', 'text-domain' ),
        'new_item'           => __( 'Nuevo Documento', 'text-domain' ),
        'update_item'         => __( 'Actualizar Documento'),
        'edit_item'          => __( 'Editar Documento', 'text-domain' ),
        'view_item'          => __( 'Ver Documento', 'text-domain' ),
        'all_items'          => __( 'Todos los Documentos', 'text-domain' ),
        'search_items'       => __( 'Buscar Documento', 'text-domain' ),
        'not_found'          => __( 'No hay Documento.', 'text-domain' ),
        'not_found_in_trash' => __( 'No hay Documento en la papelera.', 'text-domain' ),
    );

    $args_negocio = array(
        'label'               => __( 'Negocio Promotor'),
        'description'         => __( 'Area para Negocio Promotor'),
        'labels'              => $negocio,
        'menu_icon'           => __( 'dashicons-portfolio' ),
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'revisions', 'custom-fields'),
        'public'              => true,
        'hierarchical'        => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'has_archive'         => true,
        'can_export'          => true,
        'exclude_from_search' => false,
        'yarpp_support'       => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'page'
);
    register_post_type( 'Negocio', $args_negocio);
}
add_action( 'init', 'negocio_custom_post_type', 0 );

//categoria de negocio promotor

add_action( 'init', 'negocio_custom_taxonomy', 0);

function negocio_custom_taxonomy(){

$labels_negocio =array(
  'name'=> _x('Categorias','taxonomy general name' ),
  'singular_name' => _x( 'Categoria', 'taxonomy singular name' ),
  'search_items'=> __('Buscar categorias'),
  'all_items' => __('Todas las categorias'),
  'parent_item' => __('Categoria Superior'),
  'parent_item_colon' => __('Categoria Superior:'),
  'edit_item' => __('Editar Categoria'),
  'update_item' => __('Actualizar Categoria'),
  'add_new_item' => __('Agregar nueva categoria'),
  'new_item_name'=> __('Nombre nueva categoria'),
  'menu_item'=> __('Categorias'),
);


register_taxonomy( 'Categoria_negocio', array('negocio'), 
array(
  'hierarchical' => true,
  'labels' => $labels_negocio,
  'show_ui'=> true,
  'show_admin_column'=> true,
  'query_var'=> true,
  'rewrite' => array('slug'=> 'negocio'),
) );

}





// Post personalizado Experiencia única
function experiencia_custom_post_type() {
    $experiencia = array(
        'parent_item_colon'   => __( 'Parent Experiencia'),
        'name'               => _x( 'Experiencia única', 'post type general name', 'text-domain' ),
        'singular_name'      => _x( 'Experiencia única', 'post type singular name', 'text-domain' ),
        'menu_name'          => _x( 'Experiencia única', 'admin menu', 'text-domain' ),
        'add_new'            => _x( 'Añadir nuevo', 'Documento', 'text-domain' ),
        'add_new_item'       => __( 'Añadir nuevo Documento', 'text-domain' ),
        'new_item'           => __( 'Nuevo Documento', 'text-domain' ),
        'update_item'         => __( 'Actualizar Documento'),
        'edit_item'          => __( 'Editar Documento', 'text-domain' ),
        'view_item'          => __( 'Ver Documento', 'text-domain' ),
        'all_items'          => __( 'Todos los Documentos', 'text-domain' ),
        'search_items'       => __( 'Buscar Documento', 'text-domain' ),
        'not_found'          => __( 'No hay Documento.', 'text-domain' ),
        'not_found_in_trash' => __( 'No hay Documento en la papelera.', 'text-domain' ),
    );

    $args_experiencia = array(
        'label'               => __( 'Experiencia única'),
        'description'         => __( 'Area para Experiencia única'),
        'labels'              => $experiencia,
        'menu_icon'           => __( 'dashicons-id-alt' ),
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'revisions', 'custom-fields'),
        'public'              => true,
        'hierarchical'        => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'has_archive'         => true,
        'can_export'          => true,
        'exclude_from_search' => false,
        'yarpp_support'       => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'page'
);
    register_post_type( 'experiencia', $args_experiencia );
}
add_action( 'init', 'experiencia_custom_post_type', 0 );


//categoria de simulador
add_action('init', 'experiencia_custom_taxonomy', 0 );


function experiencia_custom_taxonomy(){
  $labels_experiencia = array(
  'name' => _x('Categorias','taxonomy general name'),
  'singular_name' => _x('Categoria', 'taxonomy singular name'),
  'search_items' => __('Buscar Categorias'),
  'all_items'=> __('Todas las Categorias'),
  'parent_item' =>__('Categoria Superior'),
  'parent_item_colon' => __('Categoria Superior:'),
  'edit_item'=> __('Editar Categoria'),
  'update_item'=>__('Actualizar Categoria'),
  'add_new_item'=>__('Agregar nueva categoria'),
  'menu_name'=> __('Categorias'),

);

register_taxonomy('Categoria_experiencia', array('experiencia'),
array(
  'hierarchical' => true,
  'labels' => $labels_experiencia,
  'show_ui' => true,
  'show_admin_column' => true,
  'query_var' => true,
  'rewrite' => array( 'slug' => 'experiencia' ),

));

}




function esquema_custom_post_type() {
    $esquema = array(
        'parent_item_colon'   => __( 'Parent Esquema1'),
        'name'              => _x( 'esquema', 'post type general name', 'text-domain' ),
        'singular_name'      => _x( 'Esquema', 'post type singular name', 'text-domain' ),
        'menu_name'          => _x( 'Esquema', 'admin menu', 'text-domain' ),
        'add_new'            => _x( 'Añadir nuevo', 'Esquema', 'text-domain' ),
        'add_new_item'       => __( 'Añadir nuevo Esquema', 'text-domain' ),
        'new_item'           => __( 'Nuevo Esquema', 'text-domain' ),
        'update_item'        => __( 'Actualizar Esquema'),
        'edit_item'          => __( 'Editar Esquema', 'text-domain' ),
        'view_item'          => __( 'Ver Esquema', 'text-domain' ),
        'all_items'          => __( 'Todos los Esquema', 'text-domain' ),
        'search_items'       => __( 'Buscar Esquema', 'text-domain' ),
        'not_found'          => __( 'No hay Esquema.', 'text-domain' ),
        'not_found_in_trash' => __( 'No hay Esquema en la papelera.', 'text-domain' ),
    );

    $args_esquema = array(
        'label'               => __( 'Esquema'),
        'description'         => __( 'Area para esquema'),
        'labels'              => $esquema,
        'menu_icon'           => __( 'dashicons-performance' ),
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'revisions', 'custom-fields'),
        'public'              => true,
        'hierarchical'        => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'has_archive'         => true,
        'can_export'          => true,
        'exclude_from_search' => false,
        'yarpp_support'       => true,
      
        'publicly_queryable'  => true,
        'capability_type'     => 'page'
);
    register_post_type( 'esquema', $args_esquema );
}
add_action( 'init', 'esquema_custom_post_type', 0 );

add_action( 'init', 'esquema_custom_taxonomy', 0 );


function esquema_custom_taxonomy() {
 
  $labels_esquema = array(
    'name' => _x( 'Categorias', 'taxonomy general name' ),
    'singular_name' => _x( 'Categoria', 'taxonomy singular name' ),
    'search_items' =>  __( 'Buscar Categorias' ),
    'all_items' => __( 'Todas las Categorias' ),
    'parent_item' => __( 'Categoria Superior' ),
    'parent_item_colon' => __( 'Categoria Superior:' ),
    'edit_item' => __( 'Editar Categoria' ), 
    'update_item' => __( 'Actualizar Categoria' ),
    'add_new_item' => __( 'Agregar Nueva Categoria' ),
    'new_item_name' => __( 'Nombre Nueva Categoria ' ),
    'menu_name' => __( 'Categorias' ),
  );    
 
  register_taxonomy('Categorias_esquema',array('esquema'), array(
    'hierarchical' => true,
    'labels' => $labels_esquema,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'esquema' ),
  ));
}