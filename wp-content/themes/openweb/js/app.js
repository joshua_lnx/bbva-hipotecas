'use strict';

(function ($) {
    var hexcolor = function (rgb) {
        var decimal = rgb.match(/^rgb\((\d+),\s+(\d+),\s+(\d+)\)$/);

        delete decimal[0];

        for (var x = 1; x <= 3; ++x) {
            decimal[x] = parseInt(decimal[x]).toString(16);
            decimal[x].length === 1 ? '0'+decimal[x] : decimal[x];
        }

        return ('#'+ decimal.join('')).toLowerCase();
    };

    var lazyLoad = function($ele) {
        var url = '/wp-content/uploads/load';
        var pag = parseInt($ele.attr('data-page'));
        var next = pag + 9;
        var start = '<div class="bbva-cards card-stack"><div class="container"><div class="row"><section class="card-block">';
        var end = '</section></div></div></div>';
        var type = $ele.attr('data-type') !== '' ? $ele.attr('data-type')+'/' : '';

        $.get(url+'/'+type+$ele.attr('data-lazy')+'.json', function (d) {
            var nElements = d.length;
            var elements = d.slice(pag, (next > nElements ? nElements : next));

            if (elements.length) {
                var promises = [];
                elements.forEach(function(e) {
                    promises.push($.get(url+'/'+$ele.attr('data-post')+'/post_'+e+'.json'));
                });

                Promise.all(promises).then(function(posts) {
                    $.get('/wp-content/themes/openweb/js/tpl/item.html', function(tpl) {
                        var html = [];
                        for (var x = 0, c = posts.length; x<c; ++x) {
                            if (x === 0 || x % 3 === 0) {
                                html.push(start);
                            }

                            var item = tpl.replace(/\{\{url\}\}/g, posts[x].url);
                            item = item.replace(/\{\{thumbnail\}\}/g, (posts[x].image ? posts[x].image : ''));
                            item = item.replace(/\{\{title\}\}/g, posts[x].title);
                            item = item.replace(/\{\{category\}\}/g, posts[x].category);
                            item = item.replace(/\{\{excerpt\}\}/g, posts[x].excerpt);
                            item = item.replace(/\{\{more\}\}/g, $('.faux-link').html());
                            html.push(item);

                            if (x !== 0 && (x + 1) % 3 === 0) {
                                html.push(end);
                            }
                        }

                        if (x % 3 !== 0) {
                            html.push(end);
                        }

                        $ele.attr('data-page', pag + 9);
                        var more = $('#tpl-more').addClass('bbva-cards').prop('outerHTML');

                        if (nElements > next) {
                            html.push(more);
                        }

                        $('#tpl-more').remove();

                        $('#moreArticles').append(html.join(''));
                    });
                }).catch(function(er) {
                    console.log(er)
                });
            }
        });
    };

    $(document).ready(function() {
        $('#sidr-id-searchform #sidr-id-q').attr('placeholder', $('#search-container').attr('data-placeholder-mobile'));
        $('#searchform #q').attr('placeholder', $('#search-container').attr('data-placeholder'));

        $('.prettySocial').prettySocial();

        if ($('.pre-footer').length > 0) {
            var bg = $('.pre-footer').prev().css('backgroundColor');

            if ('transparent' === bg || 'rgba(0, 0, 0, 0)' === bg || '#ffffff' === hexcolor(bg) || 'rgba(0,0,0,0)' === bg) {
                $('.pre-footer').removeClass('bg-white');
            }
        }

        menu.init();
    });

    $(document).on('click', '#lazyload', function(ev) {
        lazyLoad($(this));
        ev.preventDefault();
    });

    var menu = {
        subclassRx: /^ow-sub-menu-item-(\d+)$/,
        mainId: 'main-menu-id',
        secondaryId: 'secondary-menu',
        subMenuClass: 'sub-menu',
        subItem: 'menu-item',
        subClass: 'ow-sub-menu-item',
        subClassIterator: 'ow-sub-menu-item-',
        backIconClass: 'bbva-coronita_return',
        activeClass: 'active',
        linkBack: '<a href="#" class="icon-link"><span class="icon bbva-coronita_return"></span></a>',
        subMenuIcon: '<span class="bbva-coronita_chev link-submenu"></span>',

        init: function() {
            $('ul.sub-menu').each(function() {
                $(this).prev('a').append(menu.subMenuIcon);
            });

            menu.bindElements();
            menu.breadCrumb();
        },

        bindElements: function() {
            $(document).on('click', '#'+menu.mainId+' li a', function(e) {
                menu.showMenu($(this), e);
            });

            $(document).on('click', '#'+menu.secondaryId+' li a', function(e) {
                menu.showSubmenu($(this), e);
            });

            $(document).on('click', '.'+menu.backIconClass, function(e) {
                menu.closeSubmenu($(this), e);
            });
        },

        getNumberSubmenu: function(classnames) {
            var rx = menu.subclassRx;
            var rs = 0;
            $.each(classnames.split(' '), function(i, e) {
                if (rx.test(e)) {
                    rs = rx.exec(e)[1];
                }
            });

            return rs;
        },

        showMenu: function($element, ev = false) {
            var $sub = $element.parents('li').children('ul.'+menu.subMenuClass).children().clone();

            if ($sub.length > 0) {
                $element.parents('ul').find('a.'+menu.activeClass).removeClass(menu.activeClass);
                $element.addClass(menu.activeClass);

                $('.secondary-nav li.menu-item').remove();
                $('.secondary-nav li:not([class="search-wrapper"])').hide();

                if ($('.secondary-nav li.ow-sub-menu-item-1').length > 0) {
                    $('.secondary-nav li.ow-sub-menu-item-1').remove();
                }

                $sub.each(function (i, e) {
                    $(e).addClass(menu.subClass+' '+menu.subClassIterator+'1');
                });

                $('#secondary-menu').prepend($sub);

                if (false !== ev) {
                    ev.preventDefault();
                    ev.stopPropagation();
                }
            }
        },

        showSubmenu: function($element, ev = false) {
            var $sub = $element.parents('li').children('ul.'+menu.subMenuClass).children().clone();

            if ($sub.length > 0) {
                var $parent = $element.parents('li');
                var $sub = $parent.children('ul.'+menu.subMenuClass).children().clone().toArray();
                var x = parseInt(menu.getNumberSubmenu($parent.attr('class'))) + 1;
                $element.parents('li').siblings('li:not([class="search-wrapper"])').hide();
                $element.parents('li').hide();
                $sub.unshift($('<li>'+menu.linkBack));
                $.each($sub, function (i, e) {
                    $(e).addClass(menu.subClass+' '+menu.subClassIterator+x);
                });
                $('#secondary-menu').prepend($sub);

                if (false !== ev) {
                    ev.preventDefault();
                }
            }
        },

        closeSubmenu: function($element, ev = false) {
            var $parent = $element.parents('li');
            var x = parseInt(menu.getNumberSubmenu($parent.attr('class')));
            $('.'+menu.subClassIterator+x).remove();
            $('.'+menu.subClassIterator+(x-1)).show();

            if (false !== ev) {
                ev.preventDefault();
            }
        },

        breadCrumb: function() {
            if ('/' !== window.location.pathname) {
                var hrf = window.location.href;
                var $e = $('.'+menu.subMenuClass+' a[href="'+hrf+'"]');
                $('#'+menu.mainId+' .'+menu.subItem+' a[href="'+hrf+'"]').addClass(menu.activeClass);

                if (1 === $e.length) {
                    var sub = $e.parents('li.'+menu.subItem).toArray().reverse();
                    sub.pop();

                    $.each(sub, function(i, v) {
                        if (0 === i) {
                            var $a = $(v).children('a');
                            menu.showMenu($a);
                        } else {
                            var $a = $('#'+$(v).attr('id')+'.'+menu.subClass).children('a');
                            menu.showSubmenu($a);
                        }
                    });

                    $('.'+menu.subClass+' a[href="'+hrf+'"]').addClass(menu.activeClass);
                }
            }
        }
    };
})(jQuery);
