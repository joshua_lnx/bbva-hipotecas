'use strict';

(function($) {

    var CookiesPolicy = {
        options: {
            cookieName: 'ow-cookie-policy',
            layerId: 'ow-cookies-policy',
            closeId: 'ow-cookies-close'
        },

        getCookieName: function() {
            return (window.location.host.replace('.', '-') + '-' + CookiesPolicy.options.cookieName);
        },

        getDomain: function() {
            var i, h, weird_cookie = 'weird_get_top_level_domain=cookie',
                hostname = document.location.hostname.split('.');
            for (i = hostname.length - 1; i >= 0; i--) {
                h = hostname.slice(i).join('.');
                document.cookie = weird_cookie + ';domain=.' + h + ';';
                if (document.cookie.indexOf(weird_cookie) > -1) {
                    document.cookie = weird_cookie.split('=')[0] + '=;domain=.' + h + ';expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    return h;
                }
            }

            return '';
        },

        hasCookiesEnabled: function () {
            var cookieEnabled = !! (navigator.cookieEnabled);
            if (typeof navigator.cookieEnabled === 'undefined' && ! cookieEnabled) {
                document.cookie = "testcookie";
                cookieEnabled = !! (document.cookie.indexOf('testcookie') != -1);
            }

            return (cookieEnabled);
        },
        
        setCookie: function(create) {
            var value = 1;
            var domain = CookiesPolicy.getDomain();
            var name = CookiesPolicy.getCookieName();
            var date;
            if (create == true) {
                date = new Date();
                date.setTime(date.getTime() + (864000000000));
            } else {
                date = 'Thu, 01 Jan 1970 00:00:01 GMT';
            }
            
            document.cookie = name + '=' + value + ';expires=' + date.toUTCString() + '; path=/; domain=' + domain;
        },

        createCookie: function() {
            CookiesPolicy.setCookie(true);
        },

        removeCookie: function() {
            CookiesPolicy.setCookie(false);
        },

        getCookie: function () {
            var c_name = CookiesPolicy.getCookieName();
            var c_start, c_end;
            if (document.cookie.length > 0) {
                c_start = document.cookie.indexOf(c_name + "=");
                if (c_start != -1) {
                    c_start = c_start + c_name.length + 1;
                    c_end = document.cookie.indexOf(";", c_start);
                    if (c_end == -1) {
                        c_end = document.cookie.length;
                    }
                    return decodeURI(document.cookie.substring(c_start, c_end));
                }
            }

            return '';
        },

        removeCookiesPolicy: function() {
            var layer = $('#'+CookiesPolicy.options.layerId);
            if (layer.length == 1) {
                CookiesPolicy.createCookie();
                layer.remove();
            }
        },

        showLayer: function() {
            $('#'+CookiesPolicy.options.layerId).show();
            $('#'+CookiesPolicy.options.closeId).click(CookiesPolicy.removeCookiesPolicy);
        },

        checkPolicy: function() {
            if (! CookiesPolicy.hasCookiesEnabled()) {
                return false;
            } else {
                var cookie = CookiesPolicy.getCookie();

                if (cookie !== '1') {
                    CookiesPolicy.showLayer();
                }

                return true;
            }
        }
    };

    $(document).ready(function() {
        CookiesPolicy.checkPolicy();
    });
})(jQuery);