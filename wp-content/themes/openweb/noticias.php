<?php
/**
    *Template Name: Plantilla de Noticias
**/
?>

<?php get_header();?>

<?php

$args = array(
    'post_type' => 'Noticias1',
    'showposts'  => '15'
);
$noticias = new WP_Query($args);
?>

<div class="container">
    <h1 class="text-center thin pt40 pb40">Últimas noticias</h1>
    <div class="row my-posts3">
        <?php while ($noticias->have_posts() ): $noticias->the_post();?>
        <div class="col-12 col-sm-6 col-md-4 mb-3">
            <div class="cards-image-noticias">
                <?php
                    $image_url = get_field('link_image');
                ?>
                <img src="<?php echo $image_url; ?>"/>
            </div>
            <div class="card-text-sec-noticias pt30">
                <h4><?php the_title(); ?></h4>
                <?php $description = get_field('description');?>
                <p> <?php echo $description; ?></p>
                <?php
                $link = get_field('link'); ?>
                <a href="<?php echo $link; ?>" target="_blank">Leer más</a>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
    <br />
</div>
<?php get_footer();?>
