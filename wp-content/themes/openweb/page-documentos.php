

<?php

/*
* Template Name: Plantilla Documentos
*/
 get_header();




 $categoria1 = 'docu1';
 $taxonomia1 = 'Categorias';
 $termino1 = 'individual';
    $args1 = array(
        'post_type' => $categoria1, // the post type
        'posts_per_page'=>'2',
        'tax_query' => array(
            array(
                'taxonomy' => $taxonomia1, // the custom vocabulary
                'field'    => 'slug',                 
                'terms'    => array($termino1),      // provide the term slugs
            ),
        ),
    );



 $categoria2 = 'docu1';
 $taxonomia2 = 'Categorias';
 $termino2 = 'certificacion';
  $args2 = array(
        'post_type' => $categoria2, // the post type
        'posts_per_page'=>'2',
        'tax_query' => array(
            array(
                'taxonomy' => $taxonomia2, // the custom vocabulary
                'field'    => 'slug',                 
                'terms'    => array($termino2),      // provide the term slugs
            ),
        ),
    );






  $args3 = array(
        'post_type' => 'noticias', 
        'posts_per_page'=>'2',
     
        );


  $args4 = array(
        'post_type' => 'docu1', // the post type
        'tax_query' => array(
            array(
                'taxonomy' => 'Categorias', // the custom vocabulary
                'field'    => 'slug',                 
                'terms'    => array('modulo-de-direccion'),      // provide the term slugs
            ),
        ),
    );
 

?>



<?php get_header(); ?>



<div class="hero-banner2">
<img src="<?php echo get_the_post_thumbnail_url (); ?>">

</div>


<?php 

// First, initialize how many posts to render per page
$display_count2 = 2;

// Next, get the current page
$page2 = get_query_var( 'paged1' ) ? get_query_var( 'paged1' ) : 2;

// After that, calculate the offset
$offset2 = ( $page2 - 1 ) * $display_count2;


 $categoria2 = 'docu1';
 $taxonomia2 = 'Categorias';
 $termino2 = 'certificacion';
 

// Finally, we'll set the query arguments and instantiate WP_Query
$query_args1 = array(

    'post_type' => $categoria2, // the post type
    'posts_per_page'=>'2',
    'number'     =>  $display_count2,
    'page'       =>  $page2,
    'offset'     =>  $offset2,
    'tax_query' => array(
            array(
                'taxonomy' => $taxonomia2, // the custom vocabulary
                'field'    => 'slug',                 
                'terms'    => array($termino2),      // provide the term slugs
            ),
        ),
);


/*
 * Use your query here. Remember that if you make a call to $custom->the_post()
 * you'll need to reset the post data after the loop by calling wp_reset_postdata().
 */


 ?>


<?php 

// First, initialize how many posts to render per page
$display_count = 2;

// Next, get the current page
$page = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

// After that, calculate the offset
$offset = ( $page - 1 ) * $display_count;

// Finally, we'll set the query arguments and instantiate WP_Query
$query_args = array(
  'post_type'  =>  'noticias',
  'orderby'    =>  'date',
  'order'      =>  'desc',
  'number'     =>  $display_count,
  'page'       =>  $page,
  'offset'     =>  $offset,
  'posts_per_page'=>'2',
);


/*
 * Use your query here. Remember that if you make a call to $custom->the_post()
 * you'll need to reset the post data after the loop by calling wp_reset_postdata().
 */


 ?>

    <?php 
 
   $custom_query = new WP_Query ( $query_args );
    if ( $custom_query->have_posts() ) : 
    ?>
        <div class="documento">
            <?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
                <li><?php the_title(); ?>
                 <a href="<?php the_field('descarga'); ?>"target="_blank"><i class="bbva-coronita_download pl10 descarga"></a></i></li>
            <?php endwhile; ?>
        </div>
          <?php endif; ?>

<ul class="pagination">
    <li id="previous-posts">
        <?php previous_posts_link( '<< Pagina Anterior', $custom_query->max_num_pages ); ?>
    </li>
    <li id="next-posts">
        <?php next_posts_link( 'Cargar Más >>', $custom_query->max_num_pages ); ?>
    </li>
</ul>



    <?php 
 
   $custom_query1 = new WP_Query ( $query_args1 );
    if ( $custom_query1->have_posts() ) : 
    ?>
        <div class="documento">
            <?php while ( $custom_query1->have_posts() ) : $custom_query1->the_post(); ?>
                <li><?php the_title(); ?>
                 <a href="<?php the_field('descarga'); ?>"target="_blank"><i class="bbva-coronita_download pl10 descarga"></a></i></li>
            <?php endwhile; ?>
        </div>
          <?php endif; ?>

<ul class="pagination" id="2">
    <li id="previous-posts1">
        <?php previous_posts_link( '<< Pagina Anterior 2', $custom_query1->max_num_pages ); ?>
    </li>
    <li id="next-posts1">
        <?php next_posts_link( 'Cargar Más 2>>', $custom_query1->max_num_pages ); ?>
    </li>
</ul>



<section>

    <div class="container-fluid bg-primary ">





        <div class="container my-5">
            <div class="row">
                <div class="col-sm-3">
                    <div class="content-text">
                        <div class="bbva-coronita_profile blue-medium my-6 pt30"></div>
                        
                        <h2 class="thin"> <br>Individual</h2>

                     
                    </div>
                </div>
                <div class="col-sm-9 white mx-6">
                	<div class="bg-white">
                    <div class="card-body my-3 bbva-cards"></div>

                    <h6 class="card-title mx-4 mb-4 h6 bullet categoria-9 ":after> Documentos</h6>
                    	<ul>

          
  
<!--

                            < ?php
		

									$pquery = new WP_Query ($args);
									if($pquery->have_posts()):
									while($pquery->have_posts()):
								    $pquery->the_post();


                         ?>-->


    <?php 
 
    $mas_posts = new WP_Query( $args);
    if ( $mas_posts->have_posts() ) : 
    ?>
        <div class="documento my-posts1">
            <?php while ( $mas_posts->have_posts() ) : $mas_posts->the_post(); ?>
                <li><?php the_title(); ?>
                 <a href="<?php the_field('descarga'); ?>"target="_blank"><i class="bbva-coronita_download pl10 descarga"></a></i></li>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>
    
        </ul><div class="loadmore1" style="color:black"><img style="width:24px; height:24px;" src="<?php echo get_template_directory_uri();?>/img/icons/ver-mas.png"> Ver más</div></div>
							 

                </div>


            </div>
        </div></div>
</section>


<section>

    <div class="container-fluid bg-primary ">

        <div class="container my-5">
            <div class="row">
                <div class="col-sm-3">
                    <div class="content-text">
                        <div class="bbva-coronita_agenda blue-medium my-6 pt30"></div>
                        
                        <h2 class="thin"> <br>Certificación</h2>

                     
                    </div>
                </div>
                <div class="col-sm-9 white mx-6">
                    <div class="card-body my-3 bbva-cards"></div>
<div class="bg-white">
                    <h6 class="card-title mx-4 mb-4 h6 bullet categoria-9 ":after>Documentos</h6>
                    	<ul>
               <?php 
 
    $mas_posts = new WP_Query( $args2 );
    if ( $mas_posts->have_posts() ) : 
    ?>
        <div class="documento my-posts2">
            <?php while ( $mas_posts->have_posts() ) : $mas_posts->the_post(); ?>
                <li><?php the_title(); ?>
                 <a href="<?php the_field('descarga'); ?>"target="_blank"><i class="bbva-coronita_download pl10 descarga"></a></i></li>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>
    
        </ul><div class="loadmore2" style="color:black"><img style="width:24px; height:24px;" src="<?php echo get_template_directory_uri();?>/img/icons/ver-mas.png"> Ver más</div></div>
                             

                </div>

                

            </div>
        </div></div>

</section>



<?php  if (have_posts()): ?>
	<?php   while(have_posts()): the_post(); ?>


</section>

<section>

    <div class="container-fluid bg-primary ">

        <div class="container my-5">
            <div class="row">
                <div class="col-sm-3">
                    <div class="content-text">
                        <div class="bbva-coronita_profile blue-medium my-6 pt30"></div>
                        
                        <h2 class="thin"> <br>Promotor</h2>

                     
                    </div>
                </div>
                <div class="col-sm-9 white mx-6">
                	<div class="bg-white">
                    <div class="card-body my-3 bbva-cards"></div>

                    <h6 class="card-title mx-4 mb-4 h6 bullet categoria-9 ":after> Documentos</h6>
                    <ul>

                                  <?php 
 
    $mas_posts2 = new WP_Query( $args3 );
    if ( $mas_posts2->have_posts() ) : 
    ?>
        <div class="documento my-posts3">
            <?php while ( $mas_posts2->have_posts() ) : $mas_posts2->the_post(); ?>
                <li><?php the_title(); ?>
                 <a href="<?php the_field('descarga'); ?>"target="_blank"><i class="bbva-coronita_download pl10 descarga"></a></i></li>
            <?php endwhile; ?>
        </div>
    <?php endif; ?>
    
        </ul><div class="loadmore3" style="color:black"><img style="width:24px; height:24px;" src="<?php echo get_template_directory_uri();?>/img/icons/ver-mas.png"> Ver más</div></div>
                             

                </div>

            </div>
        </div></div>

</section>



<section>

    <div class="container-fluid bg-primary">

        <div class="container my-5">
            <div class="row">
                <div class="col-sm-3">
                    <div class="content-text">
                        <div class="bbva-coronita_portfolio blue-medium my-6 pt30"></div>
                        
                        <h2 class="thin"> <br>Modulo de Dirección</h2>

                     
                    </div>
                </div>
                <div class="col-sm-9 white mx-6">
                	<div class="bg-white">
                    <div class="card-body my-3 bbva-cards"></div>

                    <h6 class="card-title mx-4 mb-4 h6 bullet categoria-9 ":after> Documentos</h6>
                    	<ul><?php
		
									$pquery = new WP_Query ($args4);
									if($pquery->have_posts()):
									while($pquery->have_posts()):
								    $pquery->the_post();




                         ?>

 <div class="documento">
                   <li><?php the_title(''); ?>
                  <a href="<?php the_field('descarga'); ?>"target="_blank"><i class="bbva-coronita_download pl10 descarga"></a></i></li>
                    </div>
                  
		<?php   
										endwhile; 	
							


										wp_reset_postdata ();	


								

										else :

										endif; 
										
								?></ul></div>
							 

                </div>

                

            </div>
        </div></div>

</section>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>




					
					
<script type="text/javascript">
var ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
var page = 2;
//var categoria = 'Noticias';
jQuery(function($) {
    $('body').on('click', '.loadmore1', function() {
        var data = {
            'action': 'cargar_mas_posts1_ajax',
            'page': page,
            'categoria':'<?php echo $categoria1; ?>',
            'tax':'<?php echo $taxonomia1; ?>',
            'term':'<?php echo $termino1; ?>',
            'security': '<?php echo wp_create_nonce("cargar_mas_posts"); ?>'
        };
 
        $.post(ajaxurl, data, function(response) {
            if(response != '') {
                $('.my-posts1').append(response);
                page++;
            } else {
                $('.loadmore1').hide();
            }
        });
    });
});

var ajaxurl1 = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
var page1 = 2;
//var categoria = 'Noticias';
jQuery(function($) {
    $('body').on('click', '.loadmore2', function() {
        var data1 = {
            'action': 'cargar_mas_posts1_ajax',
            'page': page1,
            'categoria':'<?php echo $categoria2; ?>',
            'tax':'<?php echo $taxonomia2; ?>',
            'term':'<?php echo $termino2; ?>',
            'security': '<?php echo wp_create_nonce("cargar_mas_posts"); ?>'
        };
 
        $.post(ajaxurl1, data1, function(response1) {
            if(response1 != '') {
                $('.my-posts2').append(response1);
                page1++;
            } else {
                $('.loadmore2').hide();
            }
        });
    });
});


</script>

<script type="text/javascript">
    

var page2 = 2;
var categoria2 = 'Noticias';
jQuery(function($) {
    $('body').on('click', '.loadmore3', function() {
        var data2 = {
            'action': 'cargar_mas_posts2_ajax',
            'page2': page2,
            'categoria2': categoria2,
           
        };
 
        $.post( data2, function(response2) {
            if(response2 != '') {
                $('.my-posts3').append(response2);
                page2++;
            } else {
                $('.loadmore3').hide();
            }
        });
    });
});
</script>




</div></div>


</div><!-- #primary -->
</div><!-- .wrap -->
</article>
</div>
</section>


<?php endwhile;?>
		<?php endif;?>

<?php get_footer();
