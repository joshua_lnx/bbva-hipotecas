<?php
/**
 * Template Name: Openweb - Page Register
 * Template Post Type: page
 * Template Description: Plantilla para páginas de ejemplo.
 *
 * @package OpenWeb
 * @subpackage Coronita
 * @since OpenWeb Coronita 1.0
 */

get_header();
?>

<?php
while (have_posts()) {
    the_post();
}
?>

<section class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2><?php echo the_title_attribute(); ?></h2>
                <p class="no-margin">
                    <?php echo the_content(); ?>
                </p>
            </div>

            <div class="col-xs-12">
                <hr />
            </div>

            <div class="col-xs-12">
                <div class="form-wrap">
                    <div id="errorArmadillo" class="message message-error"
                         data-msg-error-conflict="<?php _e('Ya existe un usuario con esas credenciales', 'openweb'); ?>"
                         data-msg-error-default="<?php _e('Ha ocurrido un error, inténtalo más tarde', 'openweb'); ?>">
                        <span class="icon icon-lg bbva-coronita_alert"></span>
                    </div>
                    <div id="successArmadillo" class="message message-success">
                        <span class="icon icon-lg bbva-coronita_checkmark"></span>
                        <?php _e('Usuario creado correctamente', 'openweb'); ?>
                    </div>

                    <form id="openweb-form-register" autocomplete="off" class="mod material" method="" action="">
                        <div class="row">
                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <div class="form-group form-group-bbva clearfix">
                                    <input class="form-control-bbva" name="name" id="name" type="text" required="required"
                                           data-msg-err="<?php echo __('El nombre no puede estar vacío', 'openweb'); ?>">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label for="name" class="required"><?php echo ucfirst(__('nombre', 'openweb')); ?></label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <div class="form-group form-group-bbva clearfix">
                                    <input class="form-control-bbva" name="surname" id="surname" type="text" required="required"
                                           data-msg-err="<?php echo __('Los apellidos no pueden estar vacíos', 'openweb'); ?>">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label for="surname" class="required"><?php echo ucfirst(__('apellidos', 'openweb')); ?></label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <div class="form-group form-group-bbva clearfix">
                                    <input class="form-control-bbva" name="email" id="email" type="text" required="required"
                                           data-msg-err="<?php echo __('El email no puede estar vacío', 'openweb'); ?>"
                                           data-msg-err-format="El email debe ser una cuenta de correo válida">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label for="email" class="required"><?php echo ucfirst(__('correo electrónico', 'openweb')); ?></label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <div class="form-group form-group-bbva clearfix">
                                    <input class="form-control-bbva" autocomplete="off" name="username" id="username" required="required"
                                           type="text" data-msg-err="<?php echo __('El usuario no puede estar vacío', 'openweb'); ?>">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label for="username" class="required"><?php echo ucfirst(__('usuario', 'openweb')); ?></label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <div class="form-group form-group-bbva clearfix">
                                    <input class="form-control-bbva" autocomplete="off" name="password" id="password" required="required"
                                           type="password" data-msg-err="<?php echo __('La contraseña no puede estar vacía', 'openweb'); ?>"
                                           data-msg-err-repeat="Las contraseñas deben ser iguales"
                                           data-msg-err-format="<?php _e('El formato de la contraseña no es correcto', 'openweb'); ?>">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label for="password" class="required"><?php echo ucfirst(__('contraseña', 'openweb')); ?></label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <div class="form-group form-group-bbva clearfix">
                                    <input class="form-control-bbva" autocomplete="off" name="repeat-password" id="repeat-password" required="required"
                                           type="password"
                                           data-msg-err="<?php echo __('La contraseña repetida no puede estar vacía', 'openweb'); ?>">
                                    <span class="highlight"></span>
                                    <span class="bar"></span>
                                    <label for="repeat-password" class="required"><?php echo ucfirst(__('repetir contraseña', 'openweb')); ?></label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-md-offset-2 col-md-8">
                                <div class="form-group form-group-bbva">
                                    <button class="submit btn btn-aqua" name="register" id="register" type="submit"><?php echo __('Registrar', 'openweb'); ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer();

