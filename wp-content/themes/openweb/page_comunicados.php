<?php
/**
    *Template Name: Plantilla de Comunicados
**/
?>

<?php get_header();?>

<?php

$args = array(
    'post_type' => 'post',
    'showposts'  => '15'
);
$comunicados = new WP_Query($args);
?>

<div class="container">
    <?php
    echo '<h1 class="text-center thin pt40 pb40">' . get_the_title() . '</h1>';
    ?>
    <div class="row my-posts3">
        <?php while ($comunicados->have_posts() ): $comunicados->the_post();?>
        <div class="col-12 col-sm-6 col-md-4 mb-3">
            <?php
                if( has_post_thumbnail()) {
                    the_post_thumbnail('post-thumbnails', array(
                        'class' => 'cards-image-comunicados'
                    ));
                }else {
                    the_post_thumbnail(' ');
                }
            ?>
            <div class="card-text-sec-noticias pt30">
                <h4><?php the_title(); ?></h4>
                <?php
                    echo '<p>' . the_excerpt() . '</p>';
                ?>
                <a href="<?php permalink_link(); ?>">Leer más</a>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
    <br />
</div>

<?php get_footer();?>