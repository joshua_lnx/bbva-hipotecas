<?php
/*
* Template Name: Plantilla Directorio
*/

?>

<?php get_header(); ?>
<style>


.wpDataTablesWrapper table.wpDataTable>tbody>tr>th, .wpDataTablesWrapper table.wpDataTable>tbody>tr>td{

    text-align: -webkit-center !important;

}


.col-sm-1, .col-sm-1, .col-md-1, .col-md-2, .col-sm-2{
margin-right: 4px !important;

}
.container-fluid {
    padding-top: 2em;
    padding-bottom: 0px!important;
}

.btn-tam{

  width: 132px;
}


</style>


<div class="container">

<center><h1 class="thin pt40"> Directorio hipotecario <strong>BBVA</strong></h1></center>


<div class="container-fluid">
    <div class="active "><button class="btn btn-tam col-sm-1 col-xs-12" data-toggle="tab" href="#home">Bajío</button></div>
    <div class=""><button class="btn btn-tam col-sm-1 col-xs-12" type="button" data-toggle="tab" href="#menu1">Metro</button></div>
    <div class=""><button class="btn btn-tam col-sm-2 col-xs-12" data-toggle="tab" href="#menu2">Noroeste</button></div>
    <div class=""><button class="btn btn-tam col-sm-2 col-xs-12"data-toggle="tab" href="#menu3">Noroeste</a></div>
    <div class=""><button class="btn btn-tam col-sm-2 col-xs-12" data-toggle="tab" href="#menu4">Occidente</button></div>
    <div class=""><button class="btn btn-tam col-sm-1 col-xs-12" data-toggle="tab" href="#menu5">Sur</button></div>
    <div class=""><button class="btn btn-tam col-sm-2 col-xs-12" data-toggle="tab" href="#menu6">Sureste</button></div>
    <div class=""><button class="btn btn-tam col-sm-1 col-xs-12" data-toggle="tab" href="#menu7">Staff</button></div>

</div>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">

      <?php echo do_shortcode('[wpdatatable id=6]'); ?>
    
    </div>
    <div id="menu1" class="tab-pane fade">
      <?php echo do_shortcode('[wpdatatable id=7]'); ?>
    </div>
    <div id="menu2" class="tab-pane fade">
      <?php echo do_shortcode('[wpdatatable id=8]'); ?>
    </div>
    <div id="menu3" class="tab-pane fade">
      <?php echo do_shortcode('[wpdatatable id=9]'); ?>
    </div>
    <div id="menu4" class="tab-pane fade">
      <?php echo do_shortcode('[wpdatatable id=10]'); ?>
    </div>
    <div id="menu5" class="tab-pane fade">
      <?php echo do_shortcode('[wpdatatable id=11]'); ?>
    </div>
    <div id="menu6" class="tab-pane fade">
      <?php echo do_shortcode('[wpdatatable id=16]'); ?>
    </div>
    <div id="menu7" class="tab-pane fade">
      <?php echo do_shortcode('[wpdatatable id=17]'); ?>
    </div>

  </div>
</div>




    <?php get_footer(); ?>