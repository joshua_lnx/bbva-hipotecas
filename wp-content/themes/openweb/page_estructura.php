<?php
/*
* Template Name: Plantilla Estructura
*/

?>

<?php get_header(); ?>

<body style="background: #f4f4f4;">

    <div>
        <h1 class="thin text-center pt40">Conoce a quienes conformamos el equipo</h1>
    </div>

    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <!--inicia primer card-->
                <div class="col-sm-4">
                <a type="button" data-toggle="modal" data-target=".bd-example-modal-xl1" class="pointer margin">

                    <div class="card">

                        <?php if (have_rows('card_1')) :
                            while (have_rows('card_1')) : the_row();
                                // vars
                                $titulo_1 = get_sub_field('titulo_1');
                                $imagen = get_sub_field('imagen_1');
                                $descripcion1 = get_sub_field('descripcion1');

                                ?>
                        <div id="card_1" class="container-flex">
                            <img class="cards-image" src="<?php echo $imagen['url']; ?>" alt="<?php echo $imagen['alt']; ?>" />
                        </div>
                        <div class="card-text-products pt20 pb20 pl10">
                            <h4><?php echo $titulo_1; ?></h4>
                            <p><?php echo $descripcion1; ?></p>
                        </div></a>
                        <style type="text/css">
                            #card_1 {
                                background: <?php the_sub_field('color_de_fondo_1'); ?>;
                                height: 280px;
                            }

                            .cards-image {
                                height: 280px;
                            }
                        </style>

                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <!--termina primer card-->

                <!--inicia primer modal-->
                <div class="modal fade bd-example-modal-xl1 modal-space" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl modal-dialog-centered">
                        <div class="modal-content">
                            <h1 class="thin text-center"><?php echo $titulo_1; ?></h1>
                            <div class="container-fluid">
                                <!--empieza light box-->
                                <?php if (have_rows('banca_hipotecaria')) :
                                    while (have_rows('banca_hipotecaria')) : the_row();
                                        // vars
                                        $imagen_bh = get_sub_field('imagen_1_bh');
                                        ?>


                                <img class="image" src="<?php echo $imagen_bh['url']; ?>" alt="<?php echo $imagen_bh['alt']; ?>" />
                                <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                            <modal class="">
                            <button type="button" class="btn btn-secondary btn-xs btn-blue btn-modal" data-dismiss="modal">Cerrar</button>
                            
                        </div>
                    </div>
                </div>
                <!--termina primer modal-->


                <!--Inicia segunda card-->
                <div class="col-sm-4">
                <a type="button" data-toggle="modal" data-target=".bd-example-modal-xl2" class="pointer margin">

                    <div class="card">
                        <?php if (have_rows('card_2')) :
                            while (have_rows('card_2')) : the_row();
                                // vars
                                $titulo_2 = get_sub_field('titulo_2');
                                $imagen_2 = get_sub_field('imagen_2');
                                $descripcion2 = get_sub_field('descripcion2');

                                ?>
                        <div id="card_2" class="container-flex">
                            <img class="cards-image" src="<?php echo $imagen_2['url']; ?>" alt="<?php echo $imagen_2['alt']; ?>" />
                        </div>
                        <div class="card-text-products pt20 pb20 pl10">
                            <h4><?php echo $titulo_2; ?></h4>
                            <p><?php echo $descripcion2; ?></p>
                        </div></a>
                        <style type="text/css">
                            #card_2 {
                                background: <?php the_sub_field('color_de_fondo_2');
                                                    ?>;
                                height: 280px;
                            }
                        </style>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <!--termina segunda card-->

                <!--inicia segundo modal-->
                <div class="modal fade bd-example-modal-xl2 modal-space" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <h1 class="thin text-center"><?php echo $titulo_2; ?></h1>
                            <div class="container-fluid">

                                <?php if (have_rows('desarrollo_negocio')) :
                                    while (have_rows('desarrollo_negocio')) : the_row();
                                        // vars
                                        $imagen_dn = get_sub_field('imagen_1_dn');
                                        ?>
                                <img class="pl10" src="<?php echo $imagen_dn['url']; ?>" alt="<?php echo $imagen_dn['alt']; ?>" />

                                <?php endwhile; ?>
                                <?php endif; ?>


                            </div>
                            <button type="button" class="btn btn-secondary btn-xs btn-blue btn-modal" data-dismiss="modal">Cerrar</button>

                        </div>
                    </div>
                </div>
                <!--termina segundo modal-->
                <!--Inicia tercer card-->
                <div class="col-sm-4">
                <a type="button" data-toggle="modal" data-target=".bd-example-modal-xl3" class="pointer margin">

                    <div class="card">
                        <?php if (have_rows('card_3')) :
                            while (have_rows('card_3')) : the_row();
                                // vars
                                $titulo_3 = get_sub_field('titulo_3');
                                $imagen_3 = get_sub_field('imagen_3');
                                $descripcion3 = get_sub_field('descripcion3');

                                ?>
                        <div id="card_3" class="container-flex">
                            <img class="cards-image" src="<?php echo $imagen_3['url']; ?>" alt="<?php echo $imagen_3['alt']; ?>" />
                        </div>
                        <div class="card-text-products pt20 pb20 pl10">
                            <h4><?php echo $titulo_3; ?></h4>
                            <p><?php echo $descripcion3; ?></p>
                        </div></a>
                        <style type="text/css">
                            #card_3 {
                                background: <?php the_sub_field('color_de_fondo_3');
                                                    ?>;
                                height: 280px;
                            }
                        </style>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <!--termina tercer card-->
            </div>
            <!--termina primer row-->


            <!--inicia tercer modal-->
            <div class="modal fade bd-example-modal-xl3 modal-space" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl modal-dialog-centered">
                    <div class="modal-content">
                        <h1 class="thin text-center"><?php echo $titulo_3; ?></h1>
                        <div class="container-fluid">
                            <?php if (have_rows('direccion_comercial')) :
                                while (have_rows('direccion_comercial')) : the_row();
                                    // vars
                                    $imagen_dc = get_sub_field('imagen_1_dc');
                                    ?>
                            <img class="pl10" src="<?php echo $imagen_dc['url']; ?>" alt="<?php echo $imagen_dc['alt']; ?>" />

                            <?php endwhile; ?>
                            <?php endif; ?>

                        </div>
                        <button type="button" class="btn btn-secondary btn-xs btn-blue btn-modal" data-dismiss="modal">Cerrar</button>

                    </div>
                </div>
            </div>
            <!--termina tercer modal-->

            <!-- Second Row cuarta card -->
            <div class="row">
                <div class="col-sm-4">
                <a type="button" data-toggle="modal" data-target=".bd-example-modal-xl4" class="pointer margin">

                    <div class="card">
                        <?php if (have_rows('card_4')) :
                            while (have_rows('card_4')) : the_row();
                                // vars
                                $titulo_4 = get_sub_field('titulo_4');
                                $imagen_4 = get_sub_field('imagen_4');
                                $descripcion4 = get_sub_field('descripcion4');

                                ?>
                        <div id="card_4" class="container-flex">
                            <img class="cards-image" src="<?php echo $imagen_4['url']; ?>" alt="<?php echo $imagen_4['alt']; ?>" />
                        </div>
                        <div class="card-text-products pt20 pb20 pl10">
                            <h4><?php echo $titulo_4; ?></h4>
                            <p><?php echo $descripcion4; ?></p>
                        </div></a>
                        <style type="text/css">
                            #card_4 {
                                background: <?php the_sub_field('color_de_fondo_4');
                                                    ?>;
                                height: 280px;
                            }
                        </style>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <!--termina cuarta card-->

                    <!--inicia cuarto modal-->

                    <div class="modal fade bd-example-modal-xl4 modal-space" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                                <h1 class="thin text-center"><?php echo $titulo_4; ?></h1>
                                <div class="container-fluid">

                                    <?php if (have_rows('business_intelligence')) :
                                        while (have_rows('business_intelligence')) : the_row();
                                            // vars
                                            $imagen_bi = get_sub_field('imagen_1_bi');
                                            ?>
                                    <img class="pl10" src="<?php echo $imagen_bi['url']; ?>" alt="<?php echo $imagen_bi['alt']; ?>" />

                                    <?php endwhile; ?>
                                    <?php endif; ?>


                                </div>
                                <button type="button" class="btn btn-secondary btn-xs btn-blue btn-modal" data-dismiss="modal">Cerrar</button>

                            </div>

                        </div>
                    </div>
                    <!--termina cuarto modal-->


                </div>

            </div><!-- termina Second Row -->

        </div>
        <!--fin container-->
    </div>



    <?php get_footer(); ?>