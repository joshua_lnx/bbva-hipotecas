<div class="container-fluid bg-white cont-mn-rapido text-center">
    <div class="row menu_rapido">
        <?php if (have_rows('title_menu')):
                while (have_rows('title_menu')): the_row();
                $title_1 = get_sub_field('title_1');
                $title_2 = get_sub_field('title_2');
                $title_3 = get_sub_field('title_3');
                $title_4 = get_sub_field('title_4');
                $title_5 = get_sub_field('title_5');
            ?>
        <div class="col-sm-2">
            <a href="<?php
                // id local lnx: ;
                // id revisión: 283;
                $post_id = 383;
                 the_permalink($post_id)?>">
                <div class="div-svg">
                    <svg class="" width="24" height="24" viewBox="0 0 18 24" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M9.23084 2.76923C9.23084 4.29864 7.99102 5.53846 6.46161 5.53846C4.93221 5.53846 3.69238 4.29864 3.69238 2.76923C3.69238 1.23983 4.93221 0 6.46161 0C7.99102 0 9.23084 1.23983 9.23084 2.76923ZM10.9846 3.69233C11.1692 2.76002 11.0677 1.79079 10.6892 0.923096H23.0769L20.3077 3.69233H10.9846ZM13.8461 12.9231C15.3755 12.9231 16.6154 11.6833 16.6154 10.1539C16.6154 8.62447 15.3755 7.38464 13.8461 7.38464C12.3167 7.38464 11.0769 8.62447 11.0769 10.1539C11.0769 11.6833 12.3167 12.9231 13.8461 12.9231ZM0 11.077V8.30774H9.61846C9.24 9.17543 9.12923 10.1447 9.32308 11.077H0ZM6.46161 20.3077C7.99102 20.3077 9.23084 19.0679 9.23084 17.5385C9.23084 16.0091 7.99102 14.7693 6.46161 14.7693C4.93221 14.7693 3.69238 16.0091 3.69238 17.5385C3.69238 19.0679 4.93221 20.3077 6.46161 20.3077ZM10.9846 18.4615C11.1692 17.5292 11.0677 16.56 10.6892 15.6923H23.0769L20.3077 18.4615H10.9846Z"
                            fill="#237ABA" />
                    </svg>
                </div>
                <p class="disclosure"><?php echo $title_1; ?></p>
            </a>
        </div>
        <div class="col-sm-2">
            <?php $post_id = 842; ?>
            <a href="<?php the_permalink($post_id);?>">
                <div class="bbva-coronita_rewards"></div>
                <p class="disclosure"><?php echo $title_2; ?></p>
            </a>
        </div>
        <div class="col-sm-2">
            <?php $post_id = 872; ?>
            <a href="<?php the_permalink($post_id);?>">
                <div class="bbva-coronita_id"></div>
                <p class="disclosure"><?php echo $title_3; ?></p>
            </a>
        </div>
        <div class="col-sm-2">
            <?php $post_id = 844; ?>
            <a href="<?php the_permalink($post_id);?>">
                <div class="div-svg">
                    <svg width="24" height="24" viewBox="0 0 18 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M15.9889 9.3C15.3893 9.1 14.6898 9 13.9903 9C9.59335 9 5.99584 12.6 5.99584 17C5.99584 18.5 6.39557 19.8 7.09508 21H0.999307C0.399723 21 0 20.6 0 20V1C0 0.4 0.399723 0 0.999307 0H14.9896C15.5892 0 15.9889 0.4 15.9889 1V9.3ZM2.99792 7V9H5.99584L7.99446 7H2.99792ZM2.99792 3V5H9.99307L11.9917 3H2.99792ZM13.9903 23C10.6926 23 7.99446 20.3 7.99446 17C7.99446 13.7 10.6926 11 13.9903 11C17.288 11 19.9861 13.7 19.9861 17C19.9861 20.3 17.288 23 13.9903 23ZM12.3914 17.8L11.7918 20L13.9903 18.8L16.1888 20L15.5892 17.8L17.4879 16.3L14.9896 16.1L13.9903 14L12.991 16.1L10.4927 16.3L12.3914 17.8Z"
                            fill="#237ABA" />
                    </svg>
                </div>
                <p class="disclosure"><?php echo $title_4; ?></p>
            </a>
        </div>
        <div class="col-sm-2">
            <a href="<?php
                // id local lnx:234;
                // id revisión: 698;
                $post_id = 698;
                 the_permalink($post_id)?>">
                <div class="div-svg">
                    <svg class="" width="18" height="24" viewBox="0 0 18 24" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M1 0H17C17.55 0 18 0.45 18 1V23C18 23.55 17.55 24 17 24H1C0.45 24 0 23.55 0 23V1C0 0.45 0.45 0 1 0ZM8 22C8 22.55 8.45 23 9 23C9.55 23 10 22.55 10 22C10 21.45 9.55 21 9 21C8.45 21 8 21.45 8 22ZM2 20H16V3H2V20ZM3.99001 12H14V14H3.99001V12ZM14 5H3.98001V10H14V5ZM3.99001 16H14V18H3.99001V16Z"
                            fill="#237ABA" />
                    </svg>
                </div>
                <p class="disclosure"><?php echo $title_5; ?></p>
            </a>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>
<!-- Procesos -->
<?php
$titulo = get_field('titulo');
?>
<div class="container-fluid bg-bbva-container">
    <h2 class="thin text-center title-process pb30"><?php echo $titulo; ?></h2>
    <div class="container">
        <div class="row">
            <div class="col-md-4  col-sm-4 bg-one">
                <div class="card-text">
                    <?php if(have_rows('card_1')):
                    while(have_rows('card_1')): the_row();
                        $title_1 = get_sub_field('title-1');
                        $description = get_sub_field('description-1');
                        $image_1 = get_sub_field('image-1');
                    ?>
                    <div class="row svg">
                        <img class="img-procesos" src="<?php echo $image_1['url']; ?>"
                            alt="<?php echo $image_1['alt']; ?>" />
                    </div>
                    <div class="text-card">
                        <p class="title-cards-colors"><?php echo $title_1;?></p>
                        <p class="text-white"> <?php echo $description; ?></p>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-4  col-sm-4 bg-two">
                <div class="card-text">
                    <?php if(have_rows('card_2')):
                    while(have_rows('card_2')): the_row();
                        $title_2 = get_sub_field('title_2');
                        $description_2 = get_sub_field('description_2');
                        $image_2 = get_sub_field('image_2');
                    ?>
                    <div class="row svg">
                        <img class="img-procesos" src="<?php echo $image_2['url']; ?>"
                            alt="<?php echo $image_2['alt']; ?>" />
                    </div>
                    <div class="text-card">
                        <p class="title-cards-colors"><?php echo $title_2;?></p>
                        <p class="text-white"> <?php echo $description_2; ?></p>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 bg-three">
                <div class="card-text">
                    <?php if(have_rows('card_3')):
                    while(have_rows('card_3')): the_row();
                        $title_3 = get_sub_field('title_3');
                        $description_3 = get_sub_field('description_3');
                        $image_3 = get_sub_field('image_3');
                    ?>
                    <div class="row svg">
                        <img class="img-procesos" src="<?php echo $image_3['url']; ?>"
                            alt="<?php echo $image_3['alt']; ?>" />
                    </div>
                    <div class="text-card">
                        <p class="title-cards-colors"><?php echo $title_3;?></p>
                        <p class="text-white"> <?php echo $description_3; ?></p>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="row pb40">
            <div class="col-md-4 col-sm-4 bg-four">
                <div class="card-text">
                    <?php if(have_rows('card_4')):
                    while(have_rows('card_4')): the_row();
                        $title_4 = get_sub_field('title_4');
                        $description_4 = get_sub_field('description_4');
                        $image_4 = get_sub_field('image_4');
                    ?>
                    <div class="row svg">
                        <img class="img-procesos" src="<?php echo $image_4['url']; ?>"
                            alt="<?php echo $image_4['alt']; ?>" />
                    </div>
                    <div class="text-card">
                        <p class="title-cards-colors"><?php echo $title_4;?></p>
                        <p class="text-white"><?php echo $description_4; ?></p>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 bg-five">
                <div class="card-text">
                    <?php if(have_rows('card_5')):
                    while(have_rows('card_5')): the_row();
                        $title_5 = get_sub_field('title_5');
                        $description_5 = get_sub_field('description_5');
                        $image_5 = get_sub_field('image_5');
                    ?>
                    <div class="row svg">
                        <img class="img-procesos" src="<?php echo $image_5['url']; ?>"
                            alt="<?php echo $image_5['alt']; ?>" />
                    </div>
                    <div class="text-card">
                        <p class="title-cards-colors"><?php echo $title_5;?></p>
                        <p class="text-white"><?php echo $description_5; ?></p>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 bg-six">
                <div class="card-text">
                    <?php if(have_rows('card_6')):
                    while(have_rows('card_6')): the_row();
                        $title_6 = get_sub_field('title_6');
                        $description_6 = get_sub_field('description_6');
                        $image_6 = get_sub_field('image_6');
                    ?>
                    <div class="row svg">
                        <img class="img-procesos" src="<?php echo $image_6['url']; ?>"
                            alt="<?php echo $image_6['alt']; ?>" />
                    </div>
                    <div class="text-card">
                        <p class="title-cards-colors"><?php echo $title_6;?></p>
                        <p class="text-white"><?php echo $description_6; ?></p>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="featuredArticles">
    <div class="articles">
        <h1 class="text-center thin pt40"> Últimas noticias </h1>
        <div class="bbva-cards bbva-cards-editorial card-stack">
            <div class="container">
                <?php
                    $queryObject = new Wp_Query( array(
                    'showposts' => 4,
                    'post_type' => array('Noticias1'),
                    'orderby' => 2,
                    ));
                    // The Loop
                    if ( $queryObject->have_posts() ) :
                    $i = 0;
                    while ( $queryObject->have_posts() ) :
                    $queryObject->the_post();
                ?>
                <?php if ( $i == 0 ) : ?>
                <div class="row">
                    <section class="card-block">
                        <div class="col-xs-12 col-md-6">
                            <a href="<?php echo the_field('link');?>" class="card-wrap" target="_blank">
                                <span class="card-img bg-grey300"
                                    style="background-image: url(<?php the_field('link_image'); ?>)"> </span>
                                <div class="card-text">
                                    <h3 class="h5"><?php the_title(); ?></h3>
                                    <?php $description = get_field('description');?>
                                    <?php
                                        echo '<p class="disclosure">' . $description . '</p>';
                                    ?>
                                </div>
                                <p class="faux-link"> Leer más</p>
                            </a>
                        </div>
                        <?php endif;
                        if ( $i == 1 ) : ?>
                        <div class="col-xs-12 col-md-6">
                            <a href="<?php the_field('link');?>" class="card-wrap card-height" target="_blank">
                                <span class="card-img bg-grey300"
                                    style="background-image: url(<?php the_field('link_image'); ?>)"> </span>
                                <div class="card-text">
                                    <h3 class="h5"><?php the_title(); ?></h3>
                                    <?php $description = get_field('description');?>
                                    <?php
                                        echo '<p class="disclosure">' . $description . '</p>';
                                    ?>
                                </div>
                                <p class="faux-link"> Leer más</p>
                            </a>
                            <?php endif; ?>
                            <?php
                            if ( $i == 2 ) : ?>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="card-text-noticias">
                                        <div class="bbva-coronita_home"></div>
                                        <h3 class="h5"><?php the_title(); ?></h3>
                                        <?php $description = get_field('description');?>
                                        <?php
                                            echo '<p class="disclosure">' . $description . '</p>';
                                        ?>
                                        <a href="<?php echo the_field('link');?>" target="_blank">
                                            <p class="faux-link"> Leer más</p>
                                        </a>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <?php if ( $i == 3 ) : ?>
                                <div class="col-md-6 col-sm-6">
                                    <div class="card-text-noticias">
                                        <div class="bbva-coronita_home"></div>
                                        <h3 class="h5"><?php the_title(); ?></h3>
                                        <?php $description = get_field('description');?>
                                        <?php
                                            echo '<p class="disclosure">' . $description . '</p>';
                                        ?>
                                        <a href="<?php echo the_field('link');?>" target="_blank">
                                            <p class="faux-link"> Leer más</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                            <?php $i++;
                                endwhile;
                                endif;
                            ?>
                    </section>
                </div>
            </div>
        </div>
        <?php
            $post_id = 493;
            // id mac lnx: 507;
            // id mac ids: 177;
            // id producción: 493;
        ?>

        <a href="<?php the_permalink($post_id)?>">
            <div class="bbva-coronita_add loadmore-icon">
                <span class="loadmore">Ver más</span>
            </div>
        </a>