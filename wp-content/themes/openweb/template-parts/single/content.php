<?php
require_once OPENWEB_THEME_PATH.'/vendor/autoload.php';

$theme = \OpenWeb\Theme::getInstance();
$authorUrl = get_author_posts_url(get_the_author_meta('ID'));
$authorName = get_the_author_meta('display_name');
$custom = get_post_custom();
?>

<style>

h1, h2{
    margin: 56px 0 0 0 !important;
}
    .page-header{

        padding-bottom: 0 !important;
        padding-top: 4px !important; 
    }
    

    .justify{
text-align: justify !important;

}
.container {
    margin-top: 0;
}


</style>
<section class="container-fluid image-header image-header-blog bg-grey300"
         style="background-image: url(<?php the_post_thumbnail_url('main-thumb'); ?>)"
         data-300="background-position: 50% 30%" data-0="background-position: 50% 50%" id="post-image">
</section>

<section class="container page-header">
    <div class="row">
        <div class="col-xs-12 col-sm-1"></div>
        <div class="col-xs-12 col-sm-10">
            <h1 class="h2"><?php the_title(); ?></h1>
        </div>
        <div class="col-xs-12 col-sm-1"></div>
    </div>
</section>



<section class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-1"></div>
            <div class="col-xs-12 col-sm-10">
                <meta itemprop="datePublished" content="<?php the_date('Y-m-d'); ?>">
                <p class="blog-date"><?php the_modified_date('l, d F Y'); ?></p>
                
                <section>
                    <div class="text parbase section" id="content">
                        <?php the_content(); ?>
                    </div>

                    <?php
                    $tags = wp_get_post_tags($post->ID, ['fields' => 'ids']);

                    if (count($tags)): $cloud = wp_tag_cloud(['include' => $tags, 'format' => 'array']); ?>
                        <ul class="openweb-cloud-tag">
                        <?php foreach ($cloud as $link): ?>
                            <li><?php echo $link; ?></li>
                        <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>

                    <?php if (isset($custom['openweb-disclaimer'])): ?>
                        <div class="text parbase section">
                            <p class="justify"><span class="disclosure"><?php echo $custom['openweb-disclaimer'][0]; ?></p>
                        </div>
                    <?php endif; ?>
                </section>
            </div>
            <div class="col-xs-12 col-sm-1"></div>
        </div>
    </div>
</section>

<?php
if (isset($custom['openweb-related'][0])) {
    $relateds = unserialize($custom['openweb-related'][0]);

    if (count($relateds)) {
        $theme->renderView('post/related', $relateds);
    }
}
